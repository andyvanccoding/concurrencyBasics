package concurrencyAPI.executingValueRetTasksWiCallAndFut;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableAndFutureExample {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(2);
        Future<Integer> sumResult = pool.submit(new SumCalculator(100_000));
        Future<Integer> factorialResult = pool.submit(new FactorialCalculator(8));

        try {
            Integer sumValue = sumResult.get();
            System.out.println("Sum value = " + sumValue);
            Integer factorialValue = factorialResult.get();
            System.out.println("Factorial value = " + factorialValue);
        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }
        pool.shutdown();
    }
}
