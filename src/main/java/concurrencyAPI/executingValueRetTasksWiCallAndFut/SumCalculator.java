package concurrencyAPI.executingValueRetTasksWiCallAndFut;

import java.util.concurrent.Callable;

public class SumCalculator implements Callable<Integer> {

    private int n;

    public SumCalculator(int n) {
        this.n = n;
    }

    @Override
    public Integer call() throws Exception {
        int sum = 0;

        for (int i = 0; i <= n; i++) {
            sum += i;
        }
        try {
            Thread.sleep(2000);
        }catch (InterruptedException ex){
            ex.printStackTrace();
        }
        return sum;
    }
}
