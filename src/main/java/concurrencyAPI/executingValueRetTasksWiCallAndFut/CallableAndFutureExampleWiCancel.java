package concurrencyAPI.executingValueRetTasksWiCallAndFut;

import java.util.concurrent.*;

public class CallableAndFutureExampleWiCancel {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(2);

        Future<Integer> factorialResult = pool.submit(new FactorialCalculator(8));

        try {

            Integer factorialValue = null;

            try {
                System.out.println("Future canceled? " + factorialResult.isCancelled());
                System.out.println("Future done? " + factorialResult.isDone());
                factorialValue = factorialResult.get(3, TimeUnit.SECONDS);

            } catch (TimeoutException ex) {
                ex.printStackTrace();
            }

            if (factorialValue != null) {

                System.out.println("Factorial Value = " + factorialValue);

            } else {

                boolean cancelled = factorialResult.cancel(true);

                System.out.println("Task cancelled? " + cancelled);
                System.out.println("Future canceled? " + factorialResult.isCancelled());
                System.out.println("Future done? " + factorialResult.isDone());
            }

        } catch (InterruptedException | ExecutionException ex) {
            ex.printStackTrace();
        }

        pool.shutdown();
    }
}
