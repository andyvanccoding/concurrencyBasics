package concurrencyAPI.runningParallel.recursiveAction;

import java.util.concurrent.RecursiveAction;
/**
 * This class illustrates how to create a ForkJoinTask that does not return
 * a result.
 * @author www.codejava.net
 */
public class ArrayTransform extends RecursiveAction {
    int[] array;
    int number;
    int threshold = 100_000;
    int start;
    int end;

    public ArrayTransform(int[] array, int number, int start, int end) {
        this.array = array;
        this.number = number;
        this.start = start;
        this.end = end;
    }

    @Override
    protected void compute() {
        if (end - start < threshold) {
            computeDirectly();
        } else {
            int middle = (end + start) / 2;
//          here we recursively divide the task to perform in 2, when the task gets under the threshold
//          it will be processed by the computeDirectly() method.
            ArrayTransform subTask1 = new ArrayTransform(array, number, start, middle);
            ArrayTransform subTask2 = new ArrayTransform(array, number, middle, end);

            invokeAll(subTask1, subTask2);

        }
    }

    private void computeDirectly() {
        for (int i = start; i < end; i++) {
            array[i] = array[i] * number;
        }
    }
}
//when all the computation is done, the array will be put together again.