package concurrencyAPI.runningParallel.experimentWithParallelism;

import java.sql.Time;
import java.time.Duration;
import java.time.Instant;
import java.time.Period;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

/**
 * This program allows you to easily test performance for ForkJoinPool
 * with different values of parallelism and threshold.
 *
 * @author www.codejava.net
 */
public class ParallelismTest {
    static final int SIZE = 10_000_000;

    static int[] array = randomArray();

    private static int[] randomArray() {
        int[] array = new int[SIZE];
        Random rnd = new Random();

        for (int i = 0; i < SIZE; i++) {
            array[i] = rnd.nextInt(100);
        }
        return array;
    }

    public static void main(String[] args) {
        int threshold = Integer.parseInt("1000"); //lower threshold means more subTasks
        int parallelism = Integer.parseInt("1"); //amount of processors allowed to use. (Don't exceed system!)

        long startTime = System.currentTimeMillis();

        ArrayCounter mainTask = new ArrayCounter(array, 0, SIZE, threshold);
        ForkJoinPool pool = new ForkJoinPool(parallelism);
        Integer evenNumberCount = pool.invoke(mainTask);

        long endTime = System.currentTimeMillis();

        System.out.println("Number of even numbers: " + evenNumberCount);

        long time = (endTime - startTime);
        System.out.println("Execution time: " + time + " ms");
    }
}
