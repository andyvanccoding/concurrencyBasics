package concurrencyAPI.fixedRate;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BeepClock implements Runnable {

    @Override
    public void run() {
        // to HEAR the beep run in Terminal!!!
        System.out.println((char) 7);
    }

    public static void main(String[] args) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        Runnable task = new BeepClock();
        int initialDelay = 2;
        int periodicDelay = 2;

        scheduler.scheduleAtFixedRate(task, initialDelay, periodicDelay, TimeUnit.SECONDS);

//        this will stop the clock after +-10 seconds
        try {
            Thread.sleep(10_000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
            scheduler.shutdown();

        } finally {
            scheduler.shutdown();
        }
    }
}
