package concurrencyAPI.threadPoolAndExecutors.counterTest;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter {
//    private int value;
//    public void increment() {
//        value++;
//    }
//
//    public void decrement() {
//        value--;
//    }
//
//    public int get() {
//        return value;
//    }

    private AtomicInteger value = new AtomicInteger();

    public void increment() {
        value.incrementAndGet();
    }

    public void decrement() {
        value.decrementAndGet();
    }

    public int get() {
        return value.get();
    }
}
