package concurrencyAPI.threadPoolAndExecutors.counterTest;

public class UpdateThread implements Runnable {
    private final Counter counter;

    public UpdateThread(Counter counter) {
        this.counter = counter;
    }

    public void run() {
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException ex) { ex.printStackTrace(); }
        System.out.println(Thread.currentThread().getName());
        try {
            int count = 0;
            while (count < 10_000_000) {
                counter.increment();
                count++;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
