package concurrencyAPI.threadPoolAndExecutors.counterTest;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//NOTE: problem servicepool does not wait for thread to complete task....

public class ThreadsTest {
    static final int NUMBER_THREADS = 100;

    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        System.out.println("Initial Counter = " + counter.get());

//        UpdateThread[] threads = new UpdateThread[NUMBER_THREADS];
        ExecutorService pool = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(2);
        Instant start = Instant.now();
        for (int i = 0; i < NUMBER_THREADS; i++) {
            pool.execute(new UpdateThread(counter));
            latch.countDown();
        }

        latch.await();

//            pool.awaitTermination(30, TimeUnit.SECONDS);
//        pool.shutdownNow();

        Instant stop = Instant.now();

        Long timeElapse = Duration.between(start, stop).toSeconds();
        System.out.println("Final Counter = " + counter.get());
        System.out.printf("Processed in: %d seconds", timeElapse);

    }
}
