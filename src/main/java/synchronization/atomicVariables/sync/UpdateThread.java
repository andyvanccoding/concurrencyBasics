package synchronization.atomicVariables.sync;

public class UpdateThread extends Thread {
    private Counter counter;

    public UpdateThread(Counter counter) {
        this.counter = counter;
    }

    public void run() {
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException ex) { ex.printStackTrace(); }

        int count = 0;
        while (count < 10_000_000) {
            counter.increment();
            count++;
        }
    }
}
