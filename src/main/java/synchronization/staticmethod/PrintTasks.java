package synchronization.staticmethod;

public class PrintTasks {

    /*
     * There are 2 different locks here, an instance lock on this object
     * and a lock associated with the class itself (static).
     *
     * So when a thread is executing a synchronized static method,
     * it also blocks access to all other synchronized static methods.
     * The synchronized non-static methods are still executable by other threads.
     * It’s because synchronized static methods and synchronized non-static methods work on different locks: class lock and instance lock.
     */

    private static int countInstance = 0;
    private static int countStatic1 = 0;
    private static int countStatic2 = 0;

    public void print() {
        synchronized (this) {
            System.out.print(Thread.currentThread().getName());
            System.out.println(": Printing something  " + ++countInstance);
        }
    }

    public static void printStatic() {
        synchronized (PrintTasks.class) {
            System.out.print(Thread.currentThread().getName());
            System.out.println(": Printing from static 1  "+ ++countStatic1);
        }

    }

    public static void printStatic2() {
        synchronized (PrintTasks.class) {
            System.out.print(Thread.currentThread().getName());
            System.out.println(": Printing from static 2  "+ ++countStatic2);
        }

    }

}
