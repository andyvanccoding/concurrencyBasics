package synchronization.explicitLock.condition;

import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank {
    public static final int MAX_ACCOUNT = 10;
    public static final int MAX_AMOUNT = 10;
    public static final int INITIAL_BALANCE = 100;

    private Account[] accounts = new Account[MAX_ACCOUNT];

    private Lock bankLock;
    private Condition availableFund;

    public Bank() {
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account(INITIAL_BALANCE);
        }

        bankLock = new ReentrantLock();
        availableFund = bankLock.newCondition();
    }

    public void transfer(int from, int to, int amount) {
        bankLock.lock();

        try {
            while (accounts[from].getBalance() < amount) {
//                This causes the current thread blocks and waits,
//                which means the current thread gives up the lock
//                so other threads have chance to update the balance of this account.
                availableFund.await();
                getThreadsRunning();
                System.out.println(Thread.currentThread().getName() + " : is put on hold");
            }

            accounts[from].withdraw(amount);
            accounts[to].deposit(amount);

            String message = "%s transfered %d from %s to %s. Total balance: %d\n";
            String threadName = Thread.currentThread().getName();
            System.out.printf(message, threadName, amount, from, to, getTotalBalance());
//          The signalAll() method wakes up all threads which are currently waiting.
//          Note that it’s up to the thread scheduler decides which thread takes the turn.
            availableFund.signalAll();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bankLock.unlock();
        }
    }

    private void getThreadsRunning() {
        Set<Thread> threads = Thread.getAllStackTraces().keySet();
        System.out.println("\nname                     state     priority    type\n");
        for (Thread t : threads) {
            String name = t.getName();
            Thread.State state = t.getState();
            int priority = t.getPriority();
            String type = t.isDaemon() ? "Daemon" : "Normal";
            System.out.printf("%-20s \t %s \t %d \t %s\n", name, state, priority, type);
        }
    }

    public int getTotalBalance() {

        bankLock.lock();

        try {
            int total = 0;

            for (int i = 0; i < accounts.length; i++) {
                total += accounts[i].getBalance();
            }

            return total;
        } finally {
            bankLock.unlock();
        }
    }
}

