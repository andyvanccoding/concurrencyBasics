package diningphilosopher;

public class DiningPhilosophersDeadLockResolve {
    public static void main(String[] args) {
        Philosopher[] philosophers = new Philosopher[5];
        Object[] forks = new Object[philosophers.length];

        for (int i = 0; i < forks.length; i++) {
            forks[i] = new Object();
        }

        for (int i = 0; i < philosophers.length; i++) {
            Object leftFork = forks[i];
            Object rightFork = forks[(i + 1) % forks.length];

            if (i == philosophers.length - 1) {

                // The last philosopher picks up the right fork first
                philosophers[i] = new Philosopher(rightFork, leftFork);
            } else {
                philosophers[i] = new Philosopher(leftFork, rightFork);
            }

            Thread t
                    = new Thread(philosophers[i], "Philosopher " + (i + 1));
            t.start();
        }
    }
}
/*
Note:
*The deadlock resolve
    As we saw above, the primary reason for a deadlock is the circular wait
    condition where each process waits upon a resource that's being held by
    some other process. Hence, to avoid a deadlock situation we need to make
    sure that the circular wait condition is broken. There are several ways
    to achieve this, the simplest one being the follows:
        All Philosophers reach for their left fork first,
        except one who first reaches for his right fork.
 */