package diningphilosopher;

public class DiningPhilosophers {
    public static void main(String[] args) {
        Philosopher[] philosophers = new Philosopher[5];
        Object[] forks = new Object[philosophers.length];

        for (int i = 0; i < forks.length; i++) {
            forks[i] = new Object();
        }

        for (int i = 0; i < philosophers.length; i++) {
            Object leftFork = forks[i];
            Object rightFork = forks[(i + 1) % forks.length];

            philosophers[i] = new Philosopher(leftFork, rightFork);

            Thread t = new Thread(philosophers[i], "Philosopher " + (i + 1));
            t.start();
        }
    }
}
/*
Note:

This logic can cause a deadlock as is illustrated by the output:
!!only 2 philosophers can eat at a time!!
A deadlock is a situation where the progress of a system
is halted as each process is waiting to acquire a resource
held by some other process.
Philosopher 5 358055884919400: Picked up left fork
Philosopher 3 358055885906100: Picked up left fork
Philosopher 2 358055903274500: Picked up left fork
 */