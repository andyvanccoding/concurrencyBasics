package fundamentals.threadexample;

public class NumberPrintSleep implements Runnable {

    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(i);
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.out.println("I'm interrupted");
        }
    }

    public static void main(String[] args){
        Runnable task = new NumberPrintSleep();
        Thread thread = new Thread(task);
        thread.start();
    }
}
