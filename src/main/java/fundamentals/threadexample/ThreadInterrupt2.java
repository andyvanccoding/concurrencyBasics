package fundamentals.threadexample;

public class ThreadInterrupt2 implements Runnable {

    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println("This is message #" + i);
            if(Thread.interrupted()){
                System.out.println("I'm about to stop");
                return;
            }
        }
    }
    public static void main(String[] args){
        Thread t1 = new Thread(new ThreadInterrupt2());
        t1.start();
        t1.interrupt();  //this will enable the if statement to trigger

        try {
            Thread.sleep(5000);
            t1.interrupt();
        }catch (InterruptedException exception){
            //do nothing
        }
    }
}
