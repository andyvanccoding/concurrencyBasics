package fundamentals.threadPriorities;

public class ThreadPriority {
    public static void main(String[] args) {
        Thread t1 = new Thread(() ->
        {
            for (int i = 0; i < 10; i++) {
                System.out.println("Threadname: " + Thread.currentThread().getName() + " : " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }

        }
        );
        Thread t2 = new Thread(() ->
        {
            for (int i = 0; i < 10; i++) {
                System.out.println("Threadname: " + Thread.currentThread().getName() + " : " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }

        }
        );
        Thread t3 = new Thread(() ->
        {
            for (int i = 0; i < 10; i++) {
                System.out.println("Threadname: " + Thread.currentThread().getName() + " : " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }

        }
        );
        Thread t4 = new Thread(() ->
        {
            while (true) {
                System.out.println("Threadname: " + Thread.currentThread().getName() + "Running" + Thread.currentThread().isDaemon());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }
        }
        );
        t1.setName("minPriority");
        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setName("normPriority");
        t2.setPriority(Thread.NORM_PRIORITY);
        t3.setName("maxPriority");
        t3.setPriority(Thread.MAX_PRIORITY);
        t4.setName("DaemonThread");
        t4.setDaemon(true);

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }
}
